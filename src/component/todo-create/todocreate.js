import './todocreate.css';
import { useState } from 'react';

const Todocreate = (props) => {
    const [getInputTodos, setInputTodos] = useState('');

    const handleSubmit = (event) =>{
        event.preventDefault()
        const newTodo ={
            id : Math.floor(Math.random() *100) + 1,
            title: 'Learn react'
        }
        props.onCreateTodo(newTodo);
        //console.log(newTodo);
    }

    const handleInputTodo = (event) => {
        setInputTodos(event.target.value)
        console.log(getInputTodos);
    }

    return(
        <form className='todo-form' onSubmit={handleSubmit}>
            <input type='text' onChange={handleInputTodo}/>
            <button type='submit'>Add</button>
        </form>
    )
}

export default Todocreate;