import './todo.css';
import { useState } from 'react';
import Todolist from '../todo-list/todolist';
import Todocreate from '../todo-create/todocreate';

const Todo = () => {
    const [getTodos, setTodos] = useState([
        { id: 1, title: 'Beli tempe'},
        { id: 2, title: 'Masak tempe'},
        { id: 3, title: 'Makan tempe'}
    ])
    
    const eventCreateTodo = (todo) =>{
        setTodos(getTodos.concat(todo));
        console.log(getTodos);
    }

    return(
        <div>
            <h3>To do List</h3>
            <Todocreate onCreateTodo={eventCreateTodo} />
            <Todolist dataTodos={getTodos} />
        </div>
    )
}

export default Todo;