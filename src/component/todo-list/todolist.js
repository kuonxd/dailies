import './todolist.css';

const Todolist = (props) => {
    return(
        <div>
            <ul>{
                props.dataTodos.map((todo) => {
                    return <li key={todo.id}>{todo.title}</li>
                })
        }</ul>
        </div>
    )
}

export default Todolist;